# Lissajous

![](Lissajous.gif)

## Overview
Lissajous are a mathematical image that is created when simple harmonic motion has different periods in the x-direction vs the y-direction. An easy real-world example of this is the shape a pendulum draws in the sand while swinging. 
The inspiration for this game from the video [Making a physical Lissajous curve](https://www.youtube.com/watch?v=4CbPksEl51Q) by Matt Parker.

## What I have learned
* Array manipulation 

## How to Build
1.  Clone the project
2.  Build using Maven

## Usage
This project has no user interaction. If you want to adjust the shapes of the lissajous you can go into the Grid Class and adjust the last parameter of the oscillator object with scalars.

