package com.xalbec.lissajous;

import com.xalbec.lissajous.objects.Grid;
import processing.core.PApplet;

public class Main extends PApplet {

    public static void main(String[] args){

        PApplet.main(new String[] {Main.class.getName()});

    }

    public Grid grid;

    public void setup(){

        this.grid = new Grid(this);

    }

    public void settings(){

        size(800, 800);

    }

    public void draw(){

        background(0);
        grid.draw(this);
        grid.update(this);

    }

}
