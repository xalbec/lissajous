package com.xalbec.lissajous.objects;

import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;

public class Curve {

    private ArrayList<PVector> points;

    public Curve(){

        points = new ArrayList<PVector>();

    }

    public void addPoint(float x, float y){

        points.add(new PVector(x, y));

    }

    public void clearCurve(){

        points = new ArrayList<PVector>();

    }

    public void draw(PApplet parent){

        parent.stroke(255);
        parent.strokeWeight(2);
        parent.beginShape();
        for(PVector point: points){
            parent.vertex(point.x, point.y);
        }
        parent.endShape();

    }

}
