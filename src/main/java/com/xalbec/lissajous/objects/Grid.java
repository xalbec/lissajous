package com.xalbec.lissajous.objects;

import processing.core.PApplet;

import java.util.ArrayList;

public class Grid {

    //number of lissajous rows + the the oscillators on the left
    private int row = 10;
    //number of lissajous columns + the oscillators on the top
    private int col = 10;
    private ArrayList<Oscillator> topOscillators = new ArrayList<Oscillator>();
    private ArrayList<Oscillator> leftOscillators = new ArrayList<Oscillator>();
    private Curve[][] curves = new Curve[row-1][col-1];


    public Grid(PApplet parent){

        float w = parent.width;
        float h = parent.height;

        //adds all the oscillators to the array
        for(int i = 1; i < col; i++){
            topOscillators.add(new Oscillator(((1+2*i)*w)/(2*col), h/(2*row),i));
        }

        for(int i = 1; i < row; i++){
            leftOscillators.add(new Oscillator(w/(2*col), ((1+2*i)*h)/(2*row),i));
        }

        for(int i = 0; i < row-1; i++){
            for(int j = 0; j < col-1; j++){

                curves[i][j] = new Curve();

            }
        }

    }

    public void update(PApplet parent){

        for(Oscillator oscillator: topOscillators){
            oscillator.update(parent);
        }
        for (Oscillator oscillator : leftOscillators) {
            oscillator.update(parent);
        }

        for(int i = 0; i < row-1; i++){
            for(int j = 0; j < col-1; j++){

                curves[i][j].addPoint(topOscillators.get(j).x, leftOscillators.get(i).y);

            }
        }

    }

    public void draw(PApplet parent){

        for(Oscillator oscillator: topOscillators){
            oscillator.draw(parent);
        }
        for (Oscillator oscillator : leftOscillators) {
            oscillator.draw(parent);
        }

        for(int i = 0; i < row-1; i++){
            for(int j = 0; j < col-1; j++){

                curves[i][j].draw(parent);

            }
        }

    }

}
