package com.xalbec.lissajous.objects;

import processing.core.PApplet;
import processing.core.PVector;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Oscillator {

    public PVector pos;
    //these 2 values are the location of the point oscillating about the circle
    public float x;
    public float y;
    private float r = 74/2;
    private float period;
    private float dt = 0;

    public Oscillator(float posx, float posy, float period){

        pos = new PVector(posx, posy);
        this.period = period;
        this.x = pos.x + r*(float)cos((2*PI/period)*dt);
        this.y = pos.y + r*(float)sin((2*PI/period)*dt);

    }

    public void update(PApplet parent){

        dt = (float)(parent.millis())/1000f;
        this.x = pos.x + r*(float)cos((2*PI/period)*dt);
        this.y = pos.y + r*(float)sin((2*PI/period)*dt);

    }

    public void draw(PApplet parent){

        parent.strokeWeight(2);
        parent.stroke(255);
        parent.noFill();
        //width is hard programmed because its just a visual display for fun.
        parent.ellipse(pos.x, pos.y, 74, 74);
        parent.strokeWeight(10);
        parent.point(x, y);

    }

    public void setPeriod(float period){
        this.period = period;
    }

}
